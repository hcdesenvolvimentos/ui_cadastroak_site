<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define('DB_NAME', 'projetos_cadastro_site');

/** Usuário do banco de dados MySQL */
define('DB_USER', 'root');

/** Senha do banco de dados MySQL */
define('DB_PASSWORD', '');

/** Nome do host do MySQL */
define('DB_HOST', 'localhost');

/** Charset do banco de dados a ser usado na criação das tabelas. */
define('DB_CHARSET', 'utf8mb4');

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'yu/_#h;(=p,@[_Yn OGo(`?i/5u ulcnG2NkrC6QvI:*8;C]Y<,^_ORP &a;_=lG');
define('SECURE_AUTH_KEY',  'PS=$?`ja5:wpqA1$r|z3!<~Di/a)|wCM[B736S Uqh/44 dil?%mxWkT-Gx@#3<W');
define('LOGGED_IN_KEY',    '9(u~TBHA;2#y0^e.}9Tl5t{Xt.Xd_FP5^B1$=Siej.JAuFb:>+@E8a~9f18f1_z&');
define('NONCE_KEY',        '=/QRa+L1K9]e>TgLO:oQXNr.x-4 O%V@(89 [9%Y_|xahw[+^f(@yT;l+E%[gOX:');
define('AUTH_SALT',        'B_AD6J?y+R.ehU_ym{:@G.1LD|Zkm|V>:1mcnfx$0@hj|dlsolFBw{U&W+ jJ309');
define('SECURE_AUTH_SALT', '[-&16}<lRrrI*,is$:Eq-NAG*5WbMl;F@eP0pdhG?f>q0Uq}Ung@OnuQq-du,Cd)');
define('LOGGED_IN_SALT',   'jXjXYjQ/nw>wyh^:1JK]lcYcz{!gh/%;h lt&VsuB&_}OV3$ulF;!4:upsbiS#OO');
define('NONCE_SALT',       '<xVJ/9?z_K)Rn0Uo+&,1Bt 2x&7+n/jDj7,MWBqIIyi*Q>+RO#>kY+1B)}7+~DK6');

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix  = 'ak_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
