<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Amanda_Karoline



 */
global $configuracao;
get_header();
?>

		<!-- PAGINA INICIAL -->
		<div class="pg pg-inicial">
			

			<!-- SESSÃO SOBRE A PLATAFORMA -->
			<section class="aboutPlataform">
				<div class="container">
					<h6>Cadastre-se para ficar por dentro! </h6>
					<div class="row">
						<div class="col-md-6">
							<figure>
								<img src="<?php echo get_template_directory_uri(); ?>/img/unnamed.jpg" alt="Imagem">
								<img src="<?php echo get_template_directory_uri(); ?>/img/insta.jpg" alt="Imagem">
							</figure>
						</div>	
						<div class="col-md-6">
							<div class="textContent">
								<div class="formularioCadastro">
									<!--START Scripts : this is the script part you can add to the header of your theme-->
									<script type="text/javascript" src="http://localhost/projetos/cadastroAk/wp-includes/js/jquery/jquery.js?ver=2.10.2"></script>
									<script type="text/javascript" src="http://localhost/projetos/cadastroAk/wp-content/plugins/wysija-newsletters/js/validate/languages/jquery.validationEngine-pt.js?ver=2.10.2"></script>
									<script type="text/javascript" src="http://localhost/projetos/cadastroAk/wp-content/plugins/wysija-newsletters/js/validate/jquery.validationEngine.js?ver=2.10.2"></script>
									<script type="text/javascript" src="http://localhost/projetos/cadastroAk/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.10.2"></script>
									<script type="text/javascript">
										/* <![CDATA[ */
										var wysijaAJAX = {"action":"wysija_ajax","controller":"subscribers","ajaxurl":"http://localhost/projetos/cadastroAk/wp-admin/admin-ajax.php","loadingTrans":"Carregando..."};
										/* ]]> */
									</script><script type="text/javascript" src="http://localhost/projetos/cadastroAk/wp-content/plugins/wysija-newsletters/js/front-subscribers.js?ver=2.10.2"></script>
									<!--END Scripts-->

									<div class="widget_wysija_cont html_wysija"><div id="msg-form-wysija-html5bcb2eb12cddf-2" class="wysija-msg ajax"></div><form id="form-wysija-html5bcb2eb12cddf-2" method="post" action="#wysija" class="widget_wysija html_wysija">


										<input type="text" name="wysija[user][firstname]" class="wysija-input "   placeholder="Nome Completo" title="Nome"  value="" />



											<span class="abs-req">
												<input type="text" name="wysija[user][abs][firstname]" class="wysija-input validated[abs][firstname]" value="" />
											</span>



											<input type="text" name="wysija[user][email]" placeholder="e-mail" class="wysija-input validate[required,custom[email]]" title="Email"  value="" />



											<span class="abs-req">
												<input type="text" name="wysija[user][abs][email]" class="wysija-input validated[abs][email]" value="" />
											</span>



											<input type="text" name="wysija[field][cf_1]" placeholder="Cidade" class="wysija-input validate[required]" title="Cidade"  value="" />



											<span class="abs-req">
												<input type="text" name="wysija[field][abs][cf_1]" class="wysija-input validated[abs][cf_1]" value="" />
											</span>



											<input type="text" name="wysija[field][cf_3]" placeholder="Telefone" class="wysija-input validate[custom[onlyNumberSp]]" title="Telefone"  value="" />



											<span class="abs-req">
												<input type="text" name="wysija[field][abs][cf_3]" class="wysija-input validated[abs][cf_3]" value="" />
											</span>



										<input class="wysija-submit wysija-submit-field" type="submit" value="Cadastrar" />

										<input type="hidden" name="form_id" value="2" />
										<input type="hidden" name="action" value="save" />
										<input type="hidden" name="controller" value="subscribers" />
										<input type="hidden" value="1" name="wysija-page" />


										<input type="hidden" name="wysija[user_list][list_ids]" value="1" />

									</form></div>
									
								</div>
							</div>
						</div>	
					</div>
				</div>
			</section>

			<!-- SESSÃO DOS HEXAGONOS -->
			<section class="mosaicLayers">
				
			</section>

		</div>


<?php
get_footer();
