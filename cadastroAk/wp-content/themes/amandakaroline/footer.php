<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Amanda_Karoline
 */
global $configuracao;
?>	
	<!-- RODAPE -->
		<footer class="rodape">
			<div class="container">
				<div class="bordaSuperior"></div>
				<div class="row">
					<div class="col-sm-3">
						<a href="">
							<figure class="logoAk">
								<img src="<?php echo get_template_directory_uri(); ?>/img/logoFooter.png" alt="Logo ak">
							</figure>
						</a>
					</div>		
					<div class="col-sm-6">
						<p class="copyright">Todos os direitos reservados</p>
					</div>
					<div class="col-sm-3">
						<ul class="socialNetworks">
							<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/instagram.png" alt="Instagram"></a></li>
							<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="Facebook"></a></li>
							<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/pinterest.png" alt="Pinterest"></a></li>
							<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/lojarodape.png" alt="AK Black Store"></a></li>
							<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/portfolio.png" alt="AK Portfolio"></a></li>
							<li><a href=""><img src="<?php echo get_template_directory_uri(); ?>/img/blog.png" alt="AK Blog"></a></li>
						</ul>
					</div>
				</div>
			</div>
		</footer>
<?php wp_footer(); ?>

</body>
</html>
